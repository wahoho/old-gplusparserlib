package com.shiromamiru.common.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.shiromamiru.common.model.Member;

public enum MemberDao {
	INSTANCE;

	public List<Member> listMembers() {
		EntityManager em = EMFService.get().createEntityManager();
		// read the existing entries
		Query q = em.createQuery("select m from Member m");
		List<Member> members = q.getResultList();
		return members;
	}

	public void add(String name, String gplusId, String picURL) {
		synchronized (this) {
			EntityManager em = EMFService.get().createEntityManager();
			Member member = new Member(name, gplusId, picURL);
			em.persist(member);
			em.close();
		}
	}

	public void add(Member member) {
		synchronized (this) {
			EntityManager em = EMFService.get().createEntityManager();
			em.persist(member);
			em.close();
		}
	}
	
	public Member find(String gplusId) {
		EntityManager em = EMFService.get().createEntityManager();
		Query q = em.createQuery("Select m from Member m where m.gplusId = :gplusId", Member.class);
		q.setParameter("gplusId", gplusId);
		
		Member m = (Member) q.getSingleResult();
		
		return m;
	}

	public void remove(long id) {
		EntityManager em = EMFService.get().createEntityManager();
		try {
			Member member = em.find(Member.class, id);
			em.remove(member);
		} finally {
			em.close();
		}
	}
}