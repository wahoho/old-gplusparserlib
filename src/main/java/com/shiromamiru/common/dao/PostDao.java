package com.shiromamiru.common.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.google.appengine.api.datastore.Text;
import com.shiromamiru.common.model.Post;

public enum PostDao {
	INSTANCE;

	private Query setupQueryForListPost(String memberId) {
		EntityManager em = EMFService.get().createEntityManager();
		// read the existing entries
		Query q = em.createQuery("SELECT p FROM Post p WHERE p.gplusId = '" + memberId + "' ORDER BY published DESC");
		return q;
	}
	
	public Post getPost(String googlePlusId, String postId) {
		EntityManager em = EMFService.get().createEntityManager();
		// read the existing entries
		Query q = em.createQuery("SELECT p FROM Post p where p.postId = :postId and p.gplusId = :gplusId", Post.class);
		q.setParameter("gplusId", googlePlusId);
		q.setParameter("postId", postId);
		
		Post post = (Post) q.getSingleResult();
		return post;
	}

	public List<Post> listPosts() {
		EntityManager em = EMFService.get().createEntityManager();
		// read the existing entries
		Query q = em.createQuery("SELECT p FROM Post p ORDER BY published DESC");
		List<Post> posts = q.getResultList();
		return posts;
	}
	
	public List<Post> listPosts(String memberId) {
		Query q = setupQueryForListPost(memberId);
		List<Post> posts = q.getResultList();
		return posts;
	}

	public List<Post> listPosts(String memberId, int max) {
		Query q = setupQueryForListPost(memberId);
		q.setMaxResults(max);
		List<Post> posts = q.getResultList();
		return posts;
	}

	public void Post(String userId, String postId, String url, Text content,
			Date published, Date updated, Long comments, Long plusOne, Boolean hasAlbum,
			List<String> imagesList) {
		synchronized (this) {
			EntityManager em = EMFService.get().createEntityManager();
			Post post = new Post(userId, postId, url, content, published, updated,
					comments, plusOne, hasAlbum, imagesList);
			em.persist(post);
			em.close();
		}
	}

	public void add(Post post) {
		synchronized (this) {
			EntityManager em = EMFService.get().createEntityManager();
			em.persist(post);
			em.close();
		}
	}

	public void remove(long id) {
		EntityManager em = EMFService.get().createEntityManager();
		try {
			Post post = em.find(Post.class, id);
			em.remove(post);
		} finally {
			em.close();
		}
	}
	
	public void removeAll(String googlePlusId) {
		/*EntityManager em = EMFService.get().createEntityManager();
		List<Post> list = listPosts(googlePlusId);
		try {
			for (Post p : list) {
				System.out.println(p.getId() + " " + p.getPostId());
				if (p.getId() != null) {
					Post post = em.find(Post.class, p.getId());
					em.remove(post);
					Thread.sleep(1000);
				}
			}
		} catch(Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
		} finally {
			em.close();
		}
		System.out.println("Finish");*/
		
		EntityManager em = EMFService.get().createEntityManager();
		// read the existing entries
		Query q = em.createQuery("DELETE FROM Post p where p.gplusId = :gplusId");
		q.setParameter("gplusId", googlePlusId);
		q.executeUpdate();
	}
}