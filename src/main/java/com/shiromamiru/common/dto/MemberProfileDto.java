package com.shiromamiru.common.dto;

public class MemberProfileDto {
	
	private String googlePlusId;
	
	private String name;
	
	private String picture;
	
	private String profileUrl;
	
	public MemberProfileDto() { }
	
	public MemberProfileDto(String googlePlusId, String name, String picture, String profileUrl) {
		this.googlePlusId = googlePlusId;
		this.name = name;
		this.picture = picture;
		this.profileUrl = profileUrl;
	}

	/**
	 * @return the googlePlusId
	 */
	public String getGooglePlusId() {
		return googlePlusId;
	}

	/**
	 * @param googlePlusId the googlePlusId to set
	 */
	public void setGooglePlusId(String googlePlusId) {
		this.googlePlusId = googlePlusId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the picture
	 */
	public String getPicture() {
		return picture;
	}

	/**
	 * @param picture the picture to set
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}

	/**
	 * @return the profileUrl
	 */
	public String getProfileUrl() {
		return profileUrl;
	}

	/**
	 * @param profileUrl the profileUrl to set
	 */
	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}
}
