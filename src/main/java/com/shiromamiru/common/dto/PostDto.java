package com.shiromamiru.common.dto;

import java.util.ArrayList;
import java.util.List;

public class PostDto {
	
	private String googlePlusId;
	
	private String postId;
	
	private String url;
	
	private String thumbnail;
	
	private List<String> image;
	
	private String content;

	/**
	 * @return the googlePlusId
	 */
	public String getGooglePlusId() {
		return googlePlusId;
	}

	/**
	 * @param googlePlusId the googlePlusId to set
	 */
	public void setGooglePlusId(String googlePlusId) {
		this.googlePlusId = googlePlusId;
	}

	/**
	 * @return the postId
	 */
	public String getPostId() {
		return postId;
	}

	/**
	 * @param postId the postId to set
	 */
	public void setPostId(String postId) {
		this.postId = postId;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the thumbnail
	 */
	public String getThumbnail() {
		return thumbnail;
	}

	/**
	 * @param thumbnail the thumbnail to set
	 */
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	/**
	 * @return the image
	 */
	public List<String> getImage() {
		if (null == image) {
			image = new ArrayList<String>();
		}
		return image;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(List<String> image) {
		this.image = image;
	}
}
