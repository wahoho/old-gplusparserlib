package com.shiromamiru.common.dto;

import java.util.ArrayList;
import java.util.List;

public class MemberPostDto {
	
	String googlePlusId;
	
	List<PostDto> posts;

	/**
	 * @return the googlePlusId
	 */
	public String getGooglePlusId() {
		return googlePlusId;
	}

	/**
	 * @param googlePlusId the googlePlusId to set
	 */
	public void setGooglePlusId(String googlePlusId) {
		this.googlePlusId = googlePlusId;
	}

	/**
	 * @return the posts
	 */
	public List<PostDto> getPosts() {
		if (null == posts) {
			posts = new ArrayList<PostDto>();
		}
		
		return posts;
	}
}
