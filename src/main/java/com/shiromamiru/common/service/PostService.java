package com.shiromamiru.common.service;

import com.shiromamiru.common.dto.MemberPostDto;
import com.shiromamiru.common.dto.PostDto;

public interface PostService {
	
	public MemberPostDto getMemberPostList(String googlePlusId);
	
	public PostDto getPost(String googlePlusId, String postId);
	
	void deleteAllPost(String googlePlusId);

}
