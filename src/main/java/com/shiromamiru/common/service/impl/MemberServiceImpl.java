package com.shiromamiru.common.service.impl;

import org.springframework.stereotype.Service;

import com.shiromamiru.common.dao.MemberDao;
import com.shiromamiru.common.dto.MemberProfileDto;
import com.shiromamiru.common.model.Member;
import com.shiromamiru.common.service.MemberService;

@Service
public class MemberServiceImpl implements MemberService {

	public MemberProfileDto getMemberProfile(String googlePlusId) {
		
		Member m = MemberDao.INSTANCE.find(googlePlusId);
		
		// Stub Data
		MemberProfileDto member = new MemberProfileDto();
		member.setGooglePlusId(m.getGplusId());
		member.setName(m.getName());
		member.setPicture(m.getPicURL());
		member.setProfileUrl(m.getProfileURL());
		
		/*member.setGooglePlusId("103948509378248185139");
		member.setName("白間美瑠");
		member.setPicture("https://lh3.googleusercontent.com/-Fy8_nlK2BRU/UdDi_jQ_l-I/AAAAAAASJ7s/eBFDRdx52tQ/s320-no/nmb0009*.jpg");
		member.setProfileUrl("https://plus.google.com/103948509378248185139/posts");*/
		
		return member;
	}

}
