package com.shiromamiru.common.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.shiromamiru.common.dao.PostDao;
import com.shiromamiru.common.dto.MemberPostDto;
import com.shiromamiru.common.dto.PostDto;
import com.shiromamiru.common.model.Post;
import com.shiromamiru.common.service.PostService;

@Service
public class PostServiceImpl implements PostService {

	public MemberPostDto getMemberPostList(String googlePlusId) {
		PostDao dao = PostDao.INSTANCE;
		List<Post> postList = dao.listPosts(googlePlusId);
		MemberPostDto memberPost = new MemberPostDto();
		memberPost.setGooglePlusId(googlePlusId);
		
		for (Post post : postList) {
			PostDto postDto = new PostDto();
			postDto.setPostId(post.getPostId());

			List<String> imageList = post.getImagesList();
			postDto.setThumbnail(getThumbnail(imageList));

			memberPost.getPosts().add(postDto);
		}

		return memberPost;
	}

	public PostDto getPost(String googlePlusId, String postId) {
		PostDao dao = PostDao.INSTANCE;
		Post post = dao.getPost(googlePlusId, postId);

		PostDto postDto = new PostDto();

		postDto.setGooglePlusId(post.getGplusId());
		postDto.setPostId(post.getPostId());
		postDto.setUrl(post.getUrl());
		postDto.setContent(post.getContent().getValue());
		postDto.setImage(post.getImagesList());

		return postDto;
	}

	private String getThumbnail(List<String> imageList) {
		String thumbnail = "";
		if (imageList != null && !imageList.isEmpty()) {
			thumbnail = imageList.get(0);
		}
		return thumbnail;
	}

	public void deleteAllPost(String googlePlusId) {
		PostDao dao = PostDao.INSTANCE;
		dao.removeAll(googlePlusId);
	}

}
