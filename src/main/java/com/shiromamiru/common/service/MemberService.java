package com.shiromamiru.common.service;

import com.shiromamiru.common.dto.MemberProfileDto;

public interface MemberService {
	
	public MemberProfileDto getMemberProfile(String googlePlusId);

}
