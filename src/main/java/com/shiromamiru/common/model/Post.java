package com.shiromamiru.common.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.appengine.api.datastore.Text;

/**
 * Model class which will store the Post Items
 * 
 * @author Ryan Chan
 * 
 */

@Entity
public class Post {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String gplusId;
	private String postId;

	private String url;
	private Text content;
	private Date published;
	private Date updated;
	private Long comments;
	private Long plusOne;
	private boolean hasAlbum;
	private List<String> imagesList;

	public Post(String gplusId, String postId, String url, Text content,
			Date published, Date updated, Long comments, Long plusOne,
			boolean hasAlbum, List<String> imagesList) {
		this.gplusId = gplusId;
		this.postId = postId;
		this.url = url;
		this.content = content;
		this.published = published;
		this.updated = updated;
		this.comments = comments;
		this.plusOne = plusOne;
		this.hasAlbum = hasAlbum;
		this.imagesList = imagesList;
	}

	public String getGplusId() {
		return gplusId;
	}

	public void setGplusId(String gplusId) {
		this.gplusId = gplusId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Text getContent() {
		return content;
	}

	public void setContent(Text content) {
		this.content = content;
	}

	public Long getComments() {
		return comments;
	}

	public void setComments(Long comments) {
		this.comments = comments;
	}

	public Long getPlusOne() {
		return plusOne;
	}

	public void setPlusOne(Long plusOne) {
		this.plusOne = plusOne;
	}

	public boolean isHasAlbum() {
		return hasAlbum;
	}

	public void setHasAlbum(boolean hasAlbum) {
		this.hasAlbum = hasAlbum;
	}

	public List<String> getImagesList() {
		return imagesList;
	}

	public void setImagesList(List<String> imagesList) {
		this.imagesList = imagesList;
	}

	public Date getPublished() {
		return published;
	}

	public void setPublished(Date published) {
		this.published = published;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
